package com.nlmkit.korshunov_am.tm;

import com.nlmkit.korshunov_am.tm.dao.ProjectDAO;
import com.nlmkit.korshunov_am.tm.dao.TaskDAO;
import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;

import java.util.Scanner;

import static com.nlmkit.korshunov_am.tm.TerminalConst.*;

/**
 *  Тестовое приложение
 */
public class App {

    private static final ProjectDAO projectDAO = new ProjectDAO();

    private static final TaskDAO taskDAO = new TaskDAO();

    private static final Scanner scanner = new Scanner(System.in);

    static {
        projectDAO.create("P1");
        projectDAO.create("P2");

        taskDAO.create("T1");
        taskDAO.create("T2");
    }

    public static void main(String[] args) {
        run(args);
        displayWelcome();
        String command = "";
        while (!(EXIT.equals(command) || SHORT_EXIT.equals(command))) {
            command = scanner.nextLine();
            run(command);
        }
    }

    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length <1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    private static int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case SHORT_VERSION:
            case VERSION: return displayVersion();
            case SHORT_ABOUT:
            case ABOUT: return displayAbout();
            case SHORT_HELP:
            case HELP: return displayHelp();
            case SHORT_EXIT:
            case EXIT: return displayExit();

            case SHORT_PROJECT_CREATE:
            case PROJECT_CREATE: return createProject();
            case SHORT_PROJECT_CLEAR:
            case PROJECT_CLEAR: return clearProject();
            case SHORT_PROJECT_LIST:
            case PROJECT_LIST: return listProject();
            case SHORT_PROJECT_VIEW:
            case PROJECT_VIEW: return viewProjectByIndex();
            case SHORT_PROJECT_REMOVE_BY_ID:
            case PROJECT_REMOVE_BY_ID:return removeProjectByID();
            case SHORT_PROJECT_REMOVE_BY_NAME:
            case PROJECT_REMOVE_BY_NAME:return removeProjectByName();
            case SHORT_PROJECT_REMOVE_BY_INDEX:
            case PROJECT_REMOVE_BY_INDEX:return removeProjectByIndex();
            case SHORT_PROJECT_UPDATE_BY_INDEX:
            case PROJECT_UPDATE_BY_INDEX:return updateProjectByIndex();

            case SHORT_TASK_CREATE:
            case TASK_CREATE: return createTask();
            case SHORT_TASK_CLEAR:
            case TASK_CLEAR: return clearTask();
            case SHORT_TASK_LIST:
            case TASK_LIST: return listTask();
            case SHORT_TASK_VIEW:
            case TASK_VIEW: return viewTaskByIndex();
            case SHORT_TASK_REMOVE_BY_ID:
            case TASK_REMOVE_BY_ID:return removeTaskByID();
            case SHORT_TASK_REMOVE_BY_NAME:
            case TASK_REMOVE_BY_NAME:return removeTaskByName();
            case SHORT_TASK_REMOVE_BY_INDEX:
            case TASK_REMOVE_BY_INDEX:return removeTaskByIndex();
            case SHORT_TASK_UPDATE_BY_INDEX:
            case TASK_UPDATE_BY_INDEX:return updateTaskByIndex();

            default:return displayError();
        }
    }

    private static int createProject(){
        System.out.println("[CREATE PROJECT]");
        System.out.println("Please enter project name: ");
        final String name = scanner.nextLine();
        System.out.println("Please enter project description: ");
        final String description = scanner.nextLine();
        projectDAO.create(name,description);
        System.out.println("[OK]");
        return 0;
    }

    private static int updateProject(final Project project){
        System.out.println("Please enter project name: ");
        final String name = scanner.nextLine();
        System.out.println("Please enter project description: ");
        final String description = scanner.nextLine();
        projectDAO.update(project.getId(),name,description);
        System.out.println("[OK]");
        return 0;
    }

    private static int updateProjectByIndex(){
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("Please enter project index: ");
        final int index = Integer.parseInt(scanner.nextLine())-1;
        final Project project = projectDAO.findByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else updateProject(project);
        return 0;
    }

    private static int removeProjectByName(){
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("Please enter project name: ");
        final String name = scanner.nextLine();
        final Project project = projectDAO.removeByName(name);
        if (project == null) System.out.println("[FAIL]");
          else System.out.println("[OK]");
        return 0;
    }

    private static int removeProjectByID(){
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("Please enter project ID: ");
        final long id = scanner.nextLong();
        final Project project = projectDAO.removeById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    private static int removeProjectByIndex(){
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("Please enter project index: ");
        final int index = scanner.nextInt()-1;
        final Project project = projectDAO.removeByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    private static int updateTask(final Task task){
        System.out.println("Please enter task name: ");
        final String name = scanner.nextLine();
        System.out.println("Please enter task description: ");
        final String description = scanner.nextLine();
        taskDAO.update(task.getId(),name,description);
        System.out.println("[OK]");
        return 0;
    }

    private static int updateTaskByIndex(){
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("Please enter task index: ");
        final int index = Integer.parseInt(scanner.nextLine())-1;
        final Task task = taskDAO.findByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else updateTask(task);
        return 0;
    }

    private static int removeTaskByName(){
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("Please enter task name: ");
        final String name = scanner.nextLine();
        final Task task = taskDAO.removeByName(name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    private static int removeTaskByID(){
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("Please enter task ID: ");
        final long id = scanner.nextLong();
        final Task task = taskDAO.removeById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    private static int removeTaskByIndex(){
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("Please enter task index: ");
        final int index = scanner.nextInt()-1;
        final Task task = taskDAO.removeByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }


    private static int clearProject(){
        System.out.println("[CLEAR PROJECT]");
        projectDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    private static int listProject(){
        System.out.println("[LIST PROJECT]");
        int index = 1;
        for (final Project project: projectDAO.findAll()) {
            System.out.println(index + ". " + project.getId()+ ": " + project.getName());
            index ++;
        }
        System.out.println("[OK]");
        return 0;
    }

    private static int createTask(){
        System.out.println("[CREATE TASK]");
        System.out.println("Please enter task name: ");
        final String name = scanner.nextLine();
        System.out.println("Please enter task description: ");
        final String description = scanner.nextLine();
        taskDAO.create(name,description);
        System.out.println("[OK]");
        return 0;
    }

    private static int clearTask(){
        System.out.println("[CLEAR TASK]");
        taskDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    private static void viewTask(Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    private static void viewProject(Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJET]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    private static int viewTaskByIndex() {
        System.out.println("Enter, task index:");
        final  int index = scanner.nextInt() - 1;
        final  Task task = taskDAO.findByIndex(index);
        viewTask(task);
        return 0;
    }

    private static int viewProjectByIndex() {
        System.out.println("Enter, project index:");
        final  int index = scanner.nextInt() - 1;
        final  Project project = projectDAO.findByIndex(index);
        viewProject(project);
        return 0;
    }

    private static int listTask(){
        System.out.println("[LIST TASK]");

        int index = 1;
        for (final Task task: taskDAO.findAll()) {
            System.out.println(index + ". " + task.getId()+ ": " + task.getName());
            index ++;
        }

        System.out.println("[OK]");
        return 0;
    }

    private static int displayExit(){
        System.out.println("Terminate program");
        return 0;
    }

    private static int displayError(){
        System.out.println("Error! Unknown command.");
        return -1;
    }

    private static void displayWelcome(){
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        System.out.println("Enter command");
    }

    private static int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    private static int displayHelp() {
        System.out.println("Short command name in brackets");
        System.out.println("----Common commands:");
        System.out.println("version - Display version (v)");
        System.out.println("about - Display developer info (a)");
        System.out.println("help - Display list of command (h)");
        System.out.println("exit - Terminate console application (e)");
        System.out.println("----Project commands:");
        System.out.println("project-create - Create new project by name. (pcr)");
        System.out.println("project-clear - Remove all projects. (pcl)");
        System.out.println("project-list - Display list of projects. (pl)");
        System.out.println("project-view - View project info. (pv)");
        System.out.println("project-remove-by-id - Remove project by id. (prid)");
        System.out.println("project-remove-by-name - Remove project by name. (prn)");
        System.out.println("project-remove-by-index - Remove project by index. (prin)");
        System.out.println("project-update-by-index - Update name and description of project by index. (puin)");
        System.out.println("----Task commands:");
        System.out.println("task-create - Create new task by name. (tcr)");
        System.out.println("task-clear - Remove all tasks. (tcl)");
        System.out.println("task-list - Display list of tasks. (tl)");
        System.out.println("task-view - View task info. (tv)");
        System.out.println("task-remove-by-id - Remove task by id. (trid)");
        System.out.println("task-remove-by-name - Remove task by name. (trn)");
        System.out.println("task-remove-by-index - Remove task by index. (trin)");
        System.out.println("task-update-by-index - Update name and description of task by index. (tuin)");

        return 0;
    }

    private static int displayAbout() {
        System.out.println("Andrey Korshunov");
        System.out.println("korshunov_am@nlmk.com");
        return 0;
    }

}
